using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIManager : MonoBehaviour
{
    private static GameUIManager instance;
    
    void Start()
    {
        //ensuring only a single instance
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ChangePauseMenuStatus();
        }
    }

    [SerializeField] private GameObject pauseMenu, pauseMenuChild;
    [SerializeField] private GameObject[] additionalWindows;

    public void ChangePauseMenuStatus()
    {
        //Disabling all additional windows when leaving pauseMenu
        if (pauseMenu.activeInHierarchy)
        {
            for (int i = 0; i < additionalWindows.Length; i++)
            {
                additionalWindows[i].SetActive(false);
            }
        }
        else
        {
            // and ensuring childs activation
            pauseMenuChild.SetActive(true);
        }
        pauseMenu.SetActive(!pauseMenu.activeInHierarchy);        
    }
}
