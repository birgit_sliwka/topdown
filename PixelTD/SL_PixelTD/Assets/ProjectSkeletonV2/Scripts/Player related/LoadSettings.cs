using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Also used as static init
public class LoadSettings : MonoBehaviour
{
    [SerializeField] private InputOptions options;

    void Awake(){
        InputCustom.options = options;
        //InputCustom.control = InputCustom.LoadControls();
        InputCustom.control = new PlayerControls();
        InputCustom.control.Enable();
    }
    void Start()
    {
        InputCustom.LoadIDs();
        LoadGRaphics();
        LoadAudio();
    }

    [SerializeField] private GraphicalOptions toLoadGraphics;
    private void LoadGRaphics()
    {
        try
        {
            toLoadGraphics = SavingSystem<GraphicalOptions>.LoadData(toLoadGraphics, "video");

            //Resolution is saved so no manual set required

            Screen.fullScreen = toLoadGraphics.fullScreen;
            QualitySettings.vSyncCount = toLoadGraphics.vsyncamount;
        }
        catch
        {
            //No Video Options detected
        }
    }

    [SerializeField] private AudioOptions toLoadAudio;
    private void LoadAudio()
    {
        try
        {
            toLoadAudio = SavingSystem<AudioOptions>.LoadData(toLoadAudio, "audio");
            AudioListener.volume = toLoadAudio.audioMasterVolume;
        }
        catch
        {
            //NO AUDIO OPTIONS DETECTED
        }
    }

}
