using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothOnTriggerInput : MonoBehaviour
{
    [SerializeField] protected KeyCode InputCode;
    [SerializeField] string collisionString;
    protected bool canInteract;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(collisionString))
        {
            canInteract = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(collisionString))
        {
            canInteract = false;
        }
    }
}
