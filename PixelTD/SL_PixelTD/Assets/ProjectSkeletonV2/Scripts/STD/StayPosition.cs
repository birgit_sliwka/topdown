﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayPosition : MonoBehaviour
{
    [SerializeField] Transform target;
    private void Update()
    {
        transform.position = target.position;
    }
}
