using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Options/AudioOptions", order = 3)]
public class AudioOptions : ScriptableObject
{
    public float audioMasterVolume;
}
