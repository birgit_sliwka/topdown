using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public static class InputCustom
{
    public static InputOptions options;
    public static PlayerControls control;//TODO: Make this private and change all references!

    public static PlayerControls Control
    {
        get
        {
            if (control == null)
            {
                control = new PlayerControls();
                control.Enable();
            }
            return control;
        }
    }

    public static Dictionary<Guid, string> savedData;
    private static string[] ids = new string[1];
    public static bool ESCTriggered(){
        return control.UI.ESC.triggered;
    }


    public static PlayerControls LoadControls(){
        return SavingSystem<PlayerControls>.LoadData(new PlayerControls(), "Control");
    }

    public static void SaveControls(){
        PlayerControls cachedCtrl = control;
        SavingSystem<PlayerControls>.SaveThisToJson(cachedCtrl, "Control");
    }

    public static void SaveIDs()
    {
        DefineDictionary();
        ids = DeconstructDictionary();
        options.ids = ids;


        SavingSystem<InputOptions>.SaveThisToJson(options, "Input");
    }


    public static void LoadIDs()
    {
        options = SavingSystem<InputOptions>.LoadData(options, "Input");
        ids = options.ids;       
        
        ConstructDictionary();

        foreach(var map in control.asset.actionMaps)
        {
            var bindings = map.bindings;
           for(int i = 0; i < bindings.Count; i++)
           {
               if(savedData.TryGetValue(bindings[i].id, out string overridePath)){
                    map.ApplyBindingOverride(i, new InputBinding{overridePath = overridePath});
               }
           }
        }
    }

    
    private static string[] DeconstructDictionary(){

        string[] cached = new string[savedData.Count];
        int i = 0;
        foreach(var map in control.asset.actionMaps)
        {
            foreach(var bind in map.bindings){
                cached[i] = savedData[bind.id];
            }
            i++;
        }
        i = 0;

        return cached;
    }

    private static void ConstructDictionary(){
        DefineDictionary();
        int i = 0;
        foreach(var map in control.asset.actionMaps)
        {
            foreach(var bind in map.bindings){
                if(!string.IsNullOrEmpty(ids[i])){
                savedData[bind.id] = ids[i];
                }
            }
            i++;
        }
        i = 0;
    }

    private static void DefineDictionary(){

        savedData = new Dictionary<Guid, string>();
        foreach(var map in control.asset.actionMaps)
        {
            foreach(var bind in map.bindings){
                savedData[bind.id] = bind.overridePath;
            }
        }
    }
}
