using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class SavingSystem<T>
{
   public static void SaveThisToJson(T toSave, string path)
   {
        string filePath = Path.Combine(Application.persistentDataPath, (path + ".json"));
        string toSaveString = JsonUtility.ToJson(toSave);
        File.WriteAllText(filePath, toSaveString);
   }

    public static T LoadData(T input, string path)
    {
        string filePath = Path.Combine(Application.persistentDataPath, (path + ".json"));

        //deciding how to read the data properly
        if (File.Exists(filePath))
        {
            string cachedSaveString = File.ReadAllText(filePath);
           
            T cachedgeneric = input;
            if(input is MonoBehaviour || input is ScriptableObject)
            {
                JsonUtility.FromJsonOverwrite(cachedSaveString, cachedgeneric);
            }
            else
            {
              cachedgeneric = JsonUtility.FromJson<T>(cachedSaveString);
            }

            return cachedgeneric;
        }
        else
        {
            Debug.Log("No File found here" + filePath);
            return input;
        }
    }
}

