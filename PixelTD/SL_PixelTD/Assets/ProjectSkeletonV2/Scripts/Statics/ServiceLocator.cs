using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ServiceLocator
{
    public static readonly string sceneDescKey = "SceneDesc";
    public static void SaveScene(string name){
        SceneDesc sceneDescLocal = new SceneDesc(name);
        SavingSystem<SceneDesc>.SaveThisToJson(sceneDescLocal, sceneDescKey);
    }
}
