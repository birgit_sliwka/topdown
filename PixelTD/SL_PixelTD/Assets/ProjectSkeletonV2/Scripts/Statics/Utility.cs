using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{
    //A Method to play an audiosource with more control and reduced repetition
    public static void PlayDetailed(this AudioSource audioSource, AudioClip clip, float originalPitch, float pitchModification, float delay, bool loop, bool timeModification)
    {
        audioSource.clip = clip;
        audioSource.loop = loop;

        if (timeModification)
        {
            audioSource.pitch = originalPitch + Random.Range(-pitchModification, pitchModification) * Time.timeScale;
        }
        else
        {
            audioSource.pitch = originalPitch + Random.Range(-pitchModification, pitchModification);
        }


        audioSource.PlayDelayed(delay);
    }


    //A Method to play an audiosource easier, when multiple audiosources are available
    public static void PlayFast(this AudioSource audioSource, AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.pitch = (Random.Range(0.9f, 1.1f)) * Time.timeScale;
        audioSource.Play();
    }


    // A simple Chance for if statements
    public static bool Chance(int chance)
    {
        int n = Random.Range(0, 100);

        if (n <= chance) return true;
        else return false;
    }


    //Lerp but float-inaccuracy is taken in
    public static Vector3 SmoothLerp(Vector3 from, Vector3 target, float valueBuffer, float speed)
    {

        if (Vector3.Distance(from, target) > valueBuffer)
        {
            from = Vector3.Lerp(from, target, speed * 50 * Time.deltaTime);
        }
        else
        {
            from = target;
        }

        return from;
    }

    //Lerp but float-inaccuracy is taken in
    public static float SmoothLerpValue(float from, float target, float valueBuffer, float speed)
    {

        if (Mathf.Abs(from - target) > valueBuffer)
        {
            from = Mathf.Lerp(from, target, speed * 50 * Time.deltaTime);
        }
        else
        {
            from = target;
        }

        return from;
    }

    //Inaccurate wobbly lerp
    public static float WobbleLerp(float from, float target, float speed)
    {
        float x = from + Mathf.Lerp(0, (target - from), speed * TimeMultiplication());
        return x;
    }
   
    //returns all children of the object
    public static GameObject[] GetAllChildren(this Transform transform)
    {
        List<GameObject> childList = new List<GameObject>();

        for (int i = 0; i < transform.childCount; i++)
        {
            childList.Add(transform.GetChild(i).gameObject);
        }

        return childList.ToArray();
    }


    //returns a random integer
    public static int RandomNumber(int length)
    {
        int num;
        num = Random.Range(0, length);
        return num;
    }


    //Changes the hue of an color
    public static Color ChangeHue(this Color OriginalColorRGB, float H)
    {
        float localH, localS, localV;
        Color.RGBToHSV(OriginalColorRGB, out localH, out localS, out localV);

        localH = H;

        return Color.HSVToRGB(localH, localS, localV);

    }

    //rotate towards an object
    public static void RotateTowards(this Transform input, Vector3 towards, float offset)
    {
        float rotZ;

        Vector3 difference = towards - input.position;
        rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

        input.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);
    }

    //set framerate independent
    public static float TimeMultiplication()
    {
        float returnValue = Time.deltaTime * 50;
        return returnValue;
    }

    //change the timescale smoothly
    public static void SetTime(float mutliplicator)
    {
        Time.timeScale = mutliplicator;
        Time.fixedDeltaTime = 0.02f * mutliplicator;
    }


    //Sinus Time
    public static float SinTime(float multiplier)
    {
        return Mathf.Sin(Time.time * multiplier);
    }


    //Bool but with custom buffer for floating point inaccuracy
    public static bool EqualsAround(float inValue, float targetValue, float valueBuffer)
    {
        if(Mathf.Abs(inValue - targetValue) < valueBuffer)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static IEnumerator TypeString(TMPro.TextMeshProUGUI text, string target, float timeBetweenChars, bool resetTextOnStart, System.Action<int> onCharTyped, System.Action onStringTyped){
        if(resetTextOnStart) text.text = "";
        for (int i = 0; i < target.Length; i++)
        {
            yield return new WaitForSeconds(timeBetweenChars);
            text.text += target[i];
            onCharTyped?.Invoke(i);
        }
        onStringTyped?.Invoke();
    }

}
