using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonEvents : MonoBehaviour
{
    [SerializeField] private GameObject toChangeActiveStatus, toDisable;
    [SerializeField, Header("Leave blank to exit game")] private string sceneToLoad;

    public void LoadScene()
    {
        if (sceneToLoad != "")
        {
            //Make sure to destroy everything on main menu entrance and unpause Game
            if (sceneToLoad == "MainMenu")
            {
                GameObject[] allGO = FindObjectsOfType<GameObject>();
                for (int i = 0; i < allGO.Length; i++)
                {
                    Destroy(allGO[i]);
                }
            }
            SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
        }
        else
        {
            Debug.Log("Quitting Scene!");
            Application.Quit();
        }
    }

    public void ChangeGameObjectActiveStatus()
    {
        toChangeActiveStatus.SetActive(!toChangeActiveStatus.activeInHierarchy);
        if(toDisable != null)
        {
            toDisable.SetActive(!toDisable.activeInHierarchy);
        }
    }

    [SerializeField] private GameObject[] ToEnable, ToDisable;
    public void ChangeGameObjectArrayStatus()
    {
        for (int i = 0; i < ToEnable.Length; i++)
        {
            ToEnable[i].SetActive(true);
        }

        for (int i = 0; i < ToDisable.Length; i++)
        {
            ToDisable[i].SetActive(false);
        }
    }
}
