using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputMapButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private int index;
    private InputAction currentAction;


    //Write your inputs here!
    void Start()
    {    
        PlayerControls cachedCtrl = InputCustom.control;
        switch(index)
        {
            case 0:
            currentAction = cachedCtrl.UI.ESC;
            break;

            case 1:
            currentAction = cachedCtrl.GamePlay.Inventory;
            break;
        }
        text.text = InputControlPath.ToHumanReadableString(currentAction.bindings[0].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice);
    }

    public void TriggerRebind(){
        InputMapMaster.instance.RebindAction(currentAction, text);
    }
}
