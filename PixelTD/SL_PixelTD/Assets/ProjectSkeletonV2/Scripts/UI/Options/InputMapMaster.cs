using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class InputMapMaster : MonoBehaviour
{
    [SerializeField] private GameObject inputFlag;
    public static InputMapMaster instance;
    private PlayerControls control;
    private InputActionRebindingExtensions.RebindingOperation rebinding;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;    //Theoratically you could do another check to never have two instances.
        control = InputCustom.control;
    }

    public void StartListeningToInput(int index)    //Index used to determine which input to switch. Best to have a list which index equals which input!
    {
        control.UI.ESC.ToString();
    }

    public void RebindAction(InputAction toRebind, TextMeshProUGUI toChange){
        inputFlag.SetActive(true);
        toRebind.Disable();
        rebinding = toRebind.PerformInteractiveRebinding().WithControlsExcluding("Mouse").OnComplete(operation => FinishRebind(toRebind, toChange)).Start();
    }
    private void FinishRebind(InputAction input, TextMeshProUGUI toChange){
        toChange.text = InputControlPath.ToHumanReadableString(input.bindings[0].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice);
        input.Enable();
        rebinding.Dispose();
        inputFlag.SetActive(false);
    }

    void OnDisable(){
        InputCustom.SaveIDs();     //Done when closing UI do avoid saving too often
    }
}
