using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//TO DO: Add Fullscreen and Vsync toggle and "keep resolution" screen? Additionally verify this stuff in playmode/build pwease ;-;
public class ScreenMaster : MonoBehaviour
{
    //saving previous resolution in a variable for the "resolutionsaver"
    private int translatorIndex, previousIndex;
    [SerializeField] private Toggle fullScreen, vsync;
    [SerializeField] private TMP_Dropdown resolutionsDropDown;

    private List<string> dropDownStrings;
    private List<Resolution> possibleResolutions;

    private bool executeTicket = false;

    [SerializeField] private GraphicalOptions playerOptions;
    void Awake()
    {

        dropDownStrings = new List<string>();
        possibleResolutions = new List<Resolution>();

        for (int i = Screen.resolutions.Length - 1; i > 1; i--)
        {
            int localIndex = Screen.resolutions.Length - i - 1;
            possibleResolutions.Add(Screen.resolutions[i]);
            dropDownStrings.Add(Screen.resolutions[i].ToString());
        }

        //Adding all options
        resolutionsDropDown.ClearOptions();
        resolutionsDropDown.AddOptions(dropDownStrings);

        LoadData();
    }

    private void OnEnable()
    {
        //Ensuring that is it inactive. Because ESC is also in the same frame mapped to the Pause some complications may occur without this
        resolutionSaver.SetActive(false);
    }

    private void OnDisable()
    {
        if (resolutionSaver.activeInHierarchy) ResetToPrevRes();        //basicaly only happens when pressing ESC
        //Ensuring that is it inactive. Because ESC is also in the same frame mapped to the Pause some complications may occur without this
        resolutionSaver.SetActive(false);
        SaveData();
    }

    public void ChangeResolution()
    {
        int currentIndex = resolutionsDropDown.value;
        Screen.SetResolution(possibleResolutions[currentIndex].width, possibleResolutions[currentIndex].height, playerOptions.fullScreen);
        if (executeTicket) ChangeResolutionSafety(false);
        else executeTicket = true;
        translatorIndex = currentIndex;
        playerOptions.currentDropDownIndex = currentIndex;
        playerOptions.currentResolution = possibleResolutions[currentIndex];
    }

    public void ChangeFullscreen()
    {
        Screen.fullScreen = fullScreen.isOn;
        playerOptions.fullScreen = fullScreen.isOn;
    }

    //Vsync count 1 if toggle is on
    public void ChangeVsync()
    {
        int? vSynchCount = vsync.isOn ? 1 : 0;
        QualitySettings.vSyncCount = vSynchCount.Value;

        playerOptions.vsync = vsync.isOn;
        playerOptions.vsyncamount = vSynchCount.Value;
    }


    //Resolution saver related stuff. Inside this class to save some references. Because it is so closely related feels appropriate.
    [SerializeField] private GameObject resolutionSaver;
    [SerializeField] private TextMeshProUGUI resolutionSaverCountdown;
    private float internCounter;
    public void ChangeResolutionSafety(bool resetResolution)
    {
        if(resolutionSaver.activeInHierarchy)
        {
            if (resetResolution) ResetToPrevRes();
            else
            {
                //Setting the previous Index
                previousIndex = translatorIndex;
            }
        }
        else
        {
            internCounter = 10;
        }
        
        resolutionSaver.SetActive(!resolutionSaver.activeInHierarchy);
    }

   
    private void Update()
    {
        if (resolutionSaver.activeInHierarchy)
        {
            if(internCounter > 0)
            {
                resolutionSaverCountdown.text = Mathf.Floor(internCounter).ToString();
                internCounter -= Time.deltaTime;
            }
            else if (internCounter < 0)
            {
                ChangeResolutionSafety(true);
                internCounter = 0;
            }

            //when trying to exit it is changed back TODO: Change to new Input-System
           // if (Input.GetKeyDown(KeyCode.Escape))
            if(InputCustom.ESCTriggered())
            {
                ChangeResolutionSafety(true);
            }
        }
    }

    private void ResetToPrevRes()
    {
        executeTicket = false;
        Screen.SetResolution(possibleResolutions[previousIndex].width, possibleResolutions[previousIndex].height, fullScreen.isOn);
        resolutionsDropDown.value = previousIndex;
    }

    //Target framerate part //

    //currently WIP because of: Is this really needed? And when yes, what is max and what is min? reference needed!
    [SerializeField] private Slider targetFPS_Slider;
    public void ChangeTargetFramerate()
    {

    }

    public void ChangeSliderStartingValues()
    {
        
    }

    //Misc

    // Stuff to load save: Index, Vsync, Fullscreen
    private void LoadData()
    {
        playerOptions = SavingSystem<GraphicalOptions>.LoadData(playerOptions, "video");
        executeTicket = true;
        resolutionsDropDown.value = playerOptions.currentDropDownIndex;

        translatorIndex = resolutionsDropDown.value;        //as saved resolution will be loaded in the beginning, only this is necessary
        previousIndex = playerOptions.currentDropDownIndex;

        fullScreen.isOn = playerOptions.fullScreen;
        vsync.isOn = playerOptions.vsync;
    }

    private void SaveData()
    {
        SavingSystem<GraphicalOptions>.SaveThisToJson(playerOptions, "video");
    }
}
