using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonEV_StartGame : MonoBehaviour
{
    [SerializeField] private string firstSceneName = "DevRoomTD";
    public void Continue(){
        SceneDesc savedDesc = new SceneDesc("");
        savedDesc = SavingSystem<SceneDesc>.LoadData(savedDesc, ServiceLocator.sceneDescKey);
        Debug.Log(savedDesc.currentScene);
        if(savedDesc.currentScene.Length != 0){
            SceneManager.LoadScene(savedDesc.currentScene);
        }
        else{
            NewGame();
        }
    }

    public void NewGame(){
        ServiceLocator.SaveScene(firstSceneName);
        SceneManager.LoadScene(firstSceneName);
    }
}
