using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public struct SceneDesc{
    public string currentScene;
    public SceneDesc(string sceneName){
        currentScene = sceneName;
    }
}
public class CEV_Interact_Door : Interactable
{
    [SerializeField] private string loadScene;
    private void Start(){
        ev = LoadScene;
    }

    private void LoadScene(){
        SceneDesc sceneDescLocal = new SceneDesc(loadScene);
        Debug.Log(sceneDescLocal.currentScene);
        SavingSystem<SceneDesc>.SaveThisToJson(sceneDescLocal, ServiceLocator.sceneDescKey);

        UnityEngine.SceneManagement.SceneManager.LoadScene(loadScene, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
}
