using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom.Dialogue;

public class CEV_Interaction_Dialogue : Interactable
{
    [SerializeField] private Dialogue_Instance target;
    private void Start(){
        ev = TriggerDialogue;
        dEv += EndDialogue;
    }

    private void TriggerDialogue(){
        target.TriggerDialogue();
    }

    private void EndDialogue(){
        Dialogue_Handler.EndDialogueRaw(target);
    }

}
