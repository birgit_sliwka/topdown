using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEV_Interaction_SetActive : Interactable
{
    [SerializeField] private GameObject target;
    [SerializeField] private AudioSource sfx;
    private void Start(){
        ev = UpdateTarget;
    }

    public void UpdateTarget(){
        target.SetActive(!target.activeInHierarchy);
        if(sfx != null) sfx.Play();
    }
}
