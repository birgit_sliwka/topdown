using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : CustomEvent
{
    [SerializeField] private bool disableAfterInteraction, requiresInteraction = true, allowInteractionOnce = true;
    private bool allowInteraction, allowInteractionSub = true;
    private void Update(){

        if(allowInteractionOnce) if(!allowInteractionSub) return;

        if(!allowInteraction ||!requiresInteraction) return;
        if(Input.GetKeyDown(KeyCode.F)){
            Interact();
            allowInteractionSub = false;    //Set true in OnTriggerExit2D
        }
    }
    private void OnTriggerEnter2D(Collider2D input){
        if(input.CompareTag("Player")){
            allowInteraction = true;
            if(!requiresInteraction) Interact();
        }
    }

    
    private void OnTriggerExit2D(Collider2D input){
        if(input.CompareTag("Player")){
            dEv?.Invoke();
            allowInteraction = false;
            allowInteractionSub = true;
        }
    }


    private void Interact(){
        ev?.Invoke();
        if(disableAfterInteraction) this.enabled = false;
    }
}
