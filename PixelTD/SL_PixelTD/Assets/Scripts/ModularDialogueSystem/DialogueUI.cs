using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Custom.Dialogue{
    public class DialogueUI : MonoBehaviour
    {
        [SerializeField] private GameObject ui;
        private static DialogueUI instance;
        public static DialogueUI Instance { get => instance;}
        private void Awake(){
            instance = this;
            //TypeString(null, null, "Test", null);
        }
        [SerializeField] private TextMeshProUGUI sentenceText, authorText;

        public void Init(){
            StopAllCoroutines();
            sentenceText.text = "";
        }
        public void TypeSentence(Dialogue_Instance inst, Dialogue_Object obj, string tar, System.Action<Dialogue_Instance, Dialogue_Object> onTypeFinish){
            string author = obj.overrideAuthor.Length > 0? obj.overrideAuthor : inst.AuthorName;
            authorText.text = author;
            StartCoroutine(TypeSentence(inst, obj, sentenceText, tar, .1f, true, null, onTypeFinish));
        }

        
        public static IEnumerator TypeSentence(Dialogue_Instance inst, Dialogue_Object obj, TMPro.TextMeshProUGUI text, string target, float timeBetweenChars, bool resetTextOnStart, System.Action<int> onCharTyped, System.Action<Dialogue_Instance, Dialogue_Object> onStringTyped){
            if(resetTextOnStart) text.text = "";
            int i = 0;
            bool eKeyWasDown = false;
            while(i < target.Length){
                float speed = Input.GetKey(KeyCode.E)? .01f : 1f;
                if(Input.GetKey(KeyCode.F)) eKeyWasDown = true;

                text.text += target[i];
                onCharTyped?.Invoke(i);
                i++;
                yield return new WaitForSeconds(timeBetweenChars * speed);
            }

            //Wait for GetUp
            while(eKeyWasDown == true){
                eKeyWasDown = Input.GetKey(KeyCode.F);
                yield return new WaitForEndOfFrame();
            }

            //Basically wait for Input
            bool hasTyped = false;
            while(hasTyped == false){
                hasTyped = Input.GetKey(KeyCode.F);
                yield return new WaitForEndOfFrame();
            }

            onStringTyped?.Invoke(inst, obj);
        }

        public void SetUI(bool dir){
            ui.SetActive(dir);
        }

    }
}

