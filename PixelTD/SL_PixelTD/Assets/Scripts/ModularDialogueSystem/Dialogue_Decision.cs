using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Custom.Dialogue{
    [System.Serializable]public struct DialogueDecisions{
        public Dialogue_Object branch;
        public string buttonTitle;
    }
}

