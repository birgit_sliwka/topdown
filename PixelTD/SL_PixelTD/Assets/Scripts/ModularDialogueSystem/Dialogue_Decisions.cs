using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Custom.Dialogue{
    public class Dialogue_Decisions : MonoBehaviour
    {
        private static Dialogue_Decisions instance;
        public static Dialogue_Decisions Instance { get => instance; }
        
        [SerializeField] private TMP_Dropdown dropdown;
        private DialogueDecisions[] currentDecisions;
        private Dialogue_Instance dialogue_Instance;
        private void Start(){
            instance = this;
        }

        public void InitDecisions(Dialogue_Instance instance, DialogueDecisions[] decisions){
            dropdown.gameObject.SetActive(true);
            List<string> options = new List<string>();
            options.Add("Choose what to say!");
            for (int i = 0; i < decisions.Length; i++)
            {
                options.Add(decisions[i].buttonTitle);
            }
            dropdown.ClearOptions();
            dropdown.AddOptions(options);

            currentDecisions = decisions;
            dialogue_Instance = instance;
            dropdown.value = -1;
        }

        public void OnChoose(){
            if(dropdown.value == 0) return;
            int index = dropdown.value - 1;
            Dialogue_Handler.ChooseBranch(dialogue_Instance, currentDecisions[index].branch);
            dropdown.gameObject.SetActive(false);
        }

        public void SetDropDown(bool dir){
            dropdown.gameObject.SetActive(dir);
        }
    }

}
