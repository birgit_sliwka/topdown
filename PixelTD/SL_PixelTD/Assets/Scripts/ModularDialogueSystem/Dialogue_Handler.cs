using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Custom.Dialogue{
    public static class Dialogue_Handler
    {
        public static void TriggerDialogue(Dialogue_Instance instance, Dialogue_Object firstObject){
            DialogueUI.Instance.Init();
            DialogueUI.Instance.SetUI(true);
            TriggerSentence(instance, firstObject);
        }

        private static void TriggerSentence(Dialogue_Instance instance, Dialogue_Object firstObject){
            DialogueUI.Instance.TypeSentence(instance, firstObject, firstObject.sentences[instance.CurrentSentenceIndex], OnSentenceEnd);
        }

        public static void OnSentenceEnd(Dialogue_Instance instance, Dialogue_Object from){
            instance.CurrentSentenceIndex++;
            if(instance.CurrentSentenceIndex >= from.sentences.Length){
                if(from.decisions.Length > 0){
                    DisplayDecisions(instance, from.decisions);
                }else{
                    EndDialogue(instance, from);
                }
            }
            else{
                TriggerSentence(instance, from);
            }
        }

        public static void EndDialogue(Dialogue_Instance instance, Dialogue_Object from){
            instance.UseFallback = from.triggerFallback;
            instance.CurrentSentenceIndex = 0;
            DialogueUI.Instance.SetUI(false);
        }

        public static void EndDialogueRaw(Dialogue_Instance instance){
            instance.CurrentSentenceIndex = 0;
            DialogueUI.Instance.SetUI(false);
            Dialogue_Decisions.Instance.SetDropDown(false);
        }

        public static void DisplayDecisions(Dialogue_Instance instance, DialogueDecisions[] input){
            Dialogue_Decisions.Instance.InitDecisions(instance, input);
        }

        public static void ChooseBranch(Dialogue_Instance instance, Dialogue_Object chosen){
            instance.CurrentSentenceIndex = 0;
            TriggerSentence(instance, chosen);
        }
    }
}

