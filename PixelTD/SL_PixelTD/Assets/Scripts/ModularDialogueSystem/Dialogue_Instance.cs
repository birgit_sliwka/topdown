using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Custom.Dialogue{
    public class Dialogue_Instance : MonoBehaviour
    {
        [SerializeField] private Dialogue_Object startDialogue, fallback;
        [SerializeField] private string authorName;
        public string AuthorName { get => authorName; }
        private Dialogue_Object currentDialogue;
        private bool wasDisplayedFully;
        private void Start(){
            currentDialogue = startDialogue;
        }
        public void TriggerDialogue(){
            if(useFallback) currentDialogue = fallback;
            Dialogue_Handler.TriggerDialogue(this, currentDialogue);
        }

        public void OnSentenceChange(Dialogue_Object current){

        }

        public void OnDialogueChange(Dialogue_Object current){

        }

        //Mechanical variables
        private uint currentSentenceIndex;
        public uint CurrentSentenceIndex { get => currentSentenceIndex; set => currentSentenceIndex = value; }

        private bool useFallback;
        public bool UseFallback { get => useFallback; set => useFallback = value; }
    }
}

