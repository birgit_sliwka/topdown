using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Custom.Dialogue{
    [CreateAssetMenu(fileName = "Dialogue", menuName = "Dialogue/Dialogue", order = 0)]
    public class Dialogue_Object : ScriptableObject
    {
        public string overrideAuthor;
        public string[] sentences;
        public bool triggerFallback;
        public DialogueDecisions[] decisions;
    }
}

