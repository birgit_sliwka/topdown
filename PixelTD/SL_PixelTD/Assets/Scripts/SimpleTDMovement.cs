using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Custom.SL{
    public class SimpleTDMovement : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private Rigidbody2D targetRB;
        [SerializeField] private float speed = 2, sensitivity = 1;

        public void Update(){
            Move();
        }

        private void Move(){
            Vector2 inputVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * sensitivity;
            inputVector = Vector2.ClampMagnitude(inputVector, 1);
            animator.SetFloat("_xDir", inputVector.x);
            animator.SetFloat("_yDir", inputVector.y);
            animator.SetFloat("_speed", inputVector.magnitude);
            targetRB.velocity = inputVector * speed;
        }
    }
}

